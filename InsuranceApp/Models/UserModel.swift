//
//  UserModel.swift
//  InsuranceApp
//
//  Created by Thulasipathi Goalla on 8/20/21.
//

import Foundation

struct UserModel: Codable {
    let username: UserName
    let location: Location
    let email: String
    let phone: String
    let picture: Picture
    let id: Id
    
    
}

struct UserName: Codable {
    let title: String
    let name: String
}

struct Location: Codable{
    let street: Street
    let city: String
    let state: String
    let country: String
    let postcode: String
    
}

struct Street: Codable{
    let number: String
    let name: String
}

struct Picture: Codable {
    let large: String
    let medium: String
    let thumbnail: String
}

struct Id: Codable {
    let name: String
    let value: String
}
