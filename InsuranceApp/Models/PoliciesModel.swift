//
//  PoliciesModel.swift
//  InsuranceApp
//
//  Created by Thulasipathi Goalla on 8/29/21.
//

import Foundation

struct PoliciesModel : Codable {
    let title : String
    let subTitle : String
    let thumbnail : String
}
