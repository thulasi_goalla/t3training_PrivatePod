//
//  PolicyRentalTableviewCellTableViewCell.swift
//  InsuranceApp
//
//  Created by Thulasipathi Goalla on 8/29/21.
//

import UIKit

class PoliciesRentalTableViewCell: UITableViewCell {

    static var cellId = "PoliciesRentalTableViewCell"

    let titleLabel: UILabel = {
        let v = UILabel()
        v.textAlignment = .left
        v.numberOfLines = 0;
        v.sizeToFit()
        return v
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    func setupUI() {
        addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.topMargin.equalTo(10)
            make.leftMargin.equalTo(10)
            make.rightMargin.equalTo(-10)

        }
    }
    

}
