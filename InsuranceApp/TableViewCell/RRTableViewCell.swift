//
//  RRTableViewCell.swift
//  InsuranceApp
//
//  Created by Thulasipathi Goalla on 8/25/21.
//

import UIKit

class RRTableViewCell: UITableViewCell {
    
    static var cellId = "RRTableViewCell"
    
    let requestTitle: UILabel = {
        let v = UILabel()
        v.textAlignment = .left
        v.layer.cornerRadius = 5
        v.layer.masksToBounds = true
        return v
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    func setupUI() {
        addSubview(requestTitle)
        requestTitle.snp.makeConstraints { (make) in
            make.topMargin.equalTo(10)
            make.leftMargin.equalTo(10)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
