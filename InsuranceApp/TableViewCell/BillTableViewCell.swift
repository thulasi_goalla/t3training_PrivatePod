//
//  DashboardTableViewCell.swift
//  InsuranceApp
//
//  Created by Thulasipathi Goalla on 8/24/21.
//

import UIKit

class BillTableViewCell: UITableViewCell {

    static var cellId = "BillTableViewCell"
    
    let billImgView: UIImageView = {
       let imgV = UIImageView()
        imgV.layer.borderWidth = 3.0
        imgV.layer.cornerRadius = 5.0
        imgV.layer.borderColor = #colorLiteral(red: 0.4392156899, green: 0.01176470611, blue: 0.1921568662, alpha: 1).cgColor
        imgV.contentMode = UIView.ContentMode.scaleAspectFit
        return imgV
    }()
    
   
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI() {

        self.addSubview(billImgView)
        billImgView.snp.makeConstraints { (make) in
            make.topMargin.equalTo(10)
            make.bottomMargin.equalTo(-10)
            make.leftMargin.equalTo(10)
            make.rightMargin.equalTo(-10)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
}
