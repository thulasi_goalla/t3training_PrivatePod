//
//  CarouselTableViewCell.swift
//  InsuranceApp
//
//  Created by Thulasipathi Goalla on 8/24/21.
//

import UIKit

class CarouselTableViewCell: UITableViewCell, UIScrollViewDelegate {

    
    lazy var scrollView: UIScrollView = {
        let view = UIScrollView()
        view.layer.cornerRadius = 15
        view.backgroundColor = #colorLiteral(red: 0.4392156899, green: 0.01176470611, blue: 0.1921568662, alpha: 1)
        return view
    }()

    lazy var pageControl: UIPageControl = {
        let pcontrol = UIPageControl()
        return pcontrol
    }()

    var carosal:[CarouselView] = [];

    static var cellId = "CarouselTableViewCell"

    var slides:[CarouselView] = [];

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        slides = createSlides()
        setupSlideScrollView(slides: slides)
        scrollView.delegate = self;
        
    }
    
    func createSlides() -> [CarouselView] {

        let slide1 = CarouselView()
        slide1.imageView.image = UIImage(named: "ic_onboarding_1")
        
        let slide2 = CarouselView()
        slide2.imageView.image = UIImage(named: "ic_onboarding_2")
        
        let slide3 = CarouselView()
        slide3.imageView.image = UIImage(named: "ic_onboarding_3")
        return [slide1, slide2, slide3]
    }
    
    
    func setupSlideScrollView(slides : [CarouselView]) {
        
        self.contentView.addSubview(scrollView)
        scrollView.snp.makeConstraints { (make) in
            make.top.equalTo(10)
            make.leftMargin.equalTo(20)
            make.rightMargin.equalTo(-5)
            make.height.equalTo(110)

        }
        scrollView.isPagingEnabled = true
        scrollView.contentSize = CGSize(width: self.frame.width * CGFloat(slides.count), height: 110.0)
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: self.frame.width * CGFloat(i), y: 0, width: self.frame.width, height: 110.0)
            scrollView.addSubview(slides[i])
        }

        self.addSubview(pageControl)
        pageControl.snp.makeConstraints { (make) in
            make.top.equalTo(scrollView.snp.bottom).offset(10.0)
            make.leftMargin.equalTo(15)
            make.rightMargin.equalTo(-5)
        }
        pageControl.numberOfPages = slides.count
        pageControl.backgroundColor = #colorLiteral(red: 0.4392156899, green: 0.01176470611, blue: 0.1921568662, alpha: 1)
        pageControl.currentPage = 0

    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/self.frame.width)
        pageControl.currentPage = Int(pageIndex)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }    

}
