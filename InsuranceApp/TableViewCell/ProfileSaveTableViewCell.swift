//
//  ProfileSaveTableViewCell.swift
//  InsuranceApp
//
//  Created by Admin on 29/08/21.
//

import UIKit

class ProfileSaveTableViewCell: UITableViewCell {
    
    static var cellId = "profileSaveTableViewCellIdentifier"

    lazy var saveButton: UIButton = {
        let button = UIButton()
        button.setTitle("Save", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = #colorLiteral(red: 0.4392156899, green: 0.01176470611, blue: 0.1921568662, alpha: 1)
        button.layer.cornerRadius = 10
        button.addTarget(self, action: #selector(saveButtonAction), for: .touchUpInside)
        return button
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    func setupUI()  {
        self.addSubview(saveButton)
        saveButton.snp.makeConstraints { (make) in
            make.top.equalTo(0.0)
            make.leftMargin.equalTo(10)
            make.rightMargin.equalTo(-10)
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func saveButtonAction(_ sender: UIButton) {

    }

}
