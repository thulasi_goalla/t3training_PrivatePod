//
//  CustomHeadr.swift
//  InsuranceApp
//
//  Created by Thulasipathi Goalla on 8/29/21.
//

import UIKit

class CustomHeader: UIView {
    
    lazy var headerView: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.4392156899, green: 0.01176470611, blue: 0.1921568662, alpha: 1)
        return view
    }()
    
    lazy var headerLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue-Medium", size: 18)
        label.textAlignment = .center
        label.textColor = .white
        return label
    }()
    
    
    override func draw(_ rect: CGRect) {
        // Drawing code
        setUpheaderView()
    }
    
    
    private func setUpheaderView() {
        
        self.addSubview(headerView)
        headerView.snp.makeConstraints { (make) in
            make.topMargin.equalTo(0)
            make.leftMargin.equalTo(0)
            make.rightMargin.equalTo(0)
            make.height.equalTo(30)
        }
        
        headerView.addSubview(headerLabel)
        headerLabel.snp.makeConstraints { (make) in
            make.topMargin.equalTo(0)
            make.leftMargin.equalTo(0)
            make.rightMargin.equalTo(0)
            make.height.equalTo(30)
        }
    }
    
}
