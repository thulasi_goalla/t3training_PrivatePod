//
//  MyPoliciesViewController.swift
//  InsuranceApp
//
//  Created by Thulasipathi Goalla on 8/26/21..
//

import UIKit

class MyPoliciesViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    let reportCellText = ["Auto" , "Life" , "Hoouse"]
    var policiesArray: [PoliciesModel] = [PoliciesModel]()

    lazy var tableView:UITableView = {
        let v = UITableView()
        v.separatorStyle = .singleLine
        return v
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "My Policies"
        self.view.backgroundColor = UIColor.white
        let button = UIButton(type: UIButton.ButtonType.custom)
        button.setImage(UIImage(named: "Profile.png"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(profileClicked), for: .touchUpInside)
        
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItems = [barButton]
        
        self.policiesArray.append(PoliciesModel(title: "2017 RAV 4", subTitle: "Next Billing Date 08/24/2021", thumbnail: ""))
        self.policiesArray.append(PoliciesModel(title: "2019 Honda CRV", subTitle: "Next Billing Date 08/25/2021", thumbnail: ""))

        setupUI()
        // Do any additional setup after loading the view.
    }
    @objc func profileClicked()  {
        let profileViewController = ProfileViewController()
        self.navigationController?.pushViewController(profileViewController, animated: true)
    }
    
    func setupUI() {
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(RRTableViewCell.self, forCellReuseIdentifier: RRTableViewCell.cellId)
        tableView.register(AgentTableViewCell.self, forCellReuseIdentifier: AgentTableViewCell.cellId)
        tableView.register(PoliciesRentalTableViewCell.self, forCellReuseIdentifier: PoliciesRentalTableViewCell.cellId)

        
        self.view.addSubview(tableView)
        
        tableView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view.safeAreaLayoutGuide)
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return policiesArray.count
        } else if section == 2 {
            return 3
        }
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 2 {
            return 44
        }
        
        return 80.0
    }
    

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let customHeader = CustomHeader()
        if section == 0 {
            customHeader.headerLabel.text = "AUTO"
        }
        else if section == 1 {
            customHeader.headerLabel.text = "Home/Rental"
        }
        else if section == 2 {
            customHeader.headerLabel.text = "Documents"
        }
        return customHeader
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {

            let states = policiesArray[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: AgentTableViewCell.cellId, for: indexPath) as! AgentTableViewCell
            cell.agentNameLbl.text = states.title
            cell.agentNumberLbl.text = states.subTitle
            cell.accessoryType = .disclosureIndicator
//            cell.agentImgView.downloaded(from: userDataArray[indexPath.row].picture.large)
            return cell
        }else if indexPath.section  == 1 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: PoliciesRentalTableViewCell.cellId, for: indexPath) as! PoliciesRentalTableViewCell
            cell.titleLabel.text = "7000 crystoper  wen drive rental Policy end date : 01/12/2021"
            cell.accessoryType = .disclosureIndicator
            return cell

            
        }
        else if indexPath.section  == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: RRTableViewCell.cellId, for: indexPath) as! RRTableViewCell
            cell.requestTitle.text = reportCellText[indexPath.row]
            cell.accessoryType = .disclosureIndicator
            return cell
        }
        
        return UITableViewCell()
    }
    
}
