//
//  ProfileViewController.swift
//  InsuranceApp
//
//  Created by Admin on 28/08/21.
//

import UIKit
import PromiseKit
import Alamofire
import SwiftyJSON

class ProfileViewController: UITableViewController{
    
    var hiddenSections = Set<Int>()
    var userArray: [UserModel] = [UserModel]()
    var stateArray: [StateModel] = [StateModel]()
    var userDetailArray: Array = [String]()
    var isRowSelected: Bool = false
    var isRowInserted: Bool = false
    
    let stateIndexPath = 3
    var isState = "true"
    
    let stateCode = ["IL", "FL", "MA"]
    
    let messageCellText = ["Street", "Apt/Suite", "City", "State", "Country", "Zip", "Phone", "Email"]
    var detailCellText = ["1036 ibis dr","6","Bloomington","IL", "USA","62704","123-456-7891","johnpeter@gmail.com"]
    
    var tempMessageCellText = ["Street", "Apt/Suite", "City", "State", "Country", "Zip", "Phone", "Email"]
    var tempDetailCellText = ["1036 ibis dr","6","Bloomington","IL", "USA","62704","123-456-7891","johnpeter@gmail.com"]
    let pickerView = UIPickerView()
    var selectedText = ""
    
    
    @IBAction func tappedSave(_ sender: UIButton) {
        
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "MessageCell", bundle: nil), forCellReuseIdentifier: "ReusableCell")
        tableView.register(UINib(nibName: "DetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "ReusableCellOne")
        self.navigationItem.title = "PROFILE"
        let imageView = UIImageView(image: UIImage(named: "Contact"))
        let buttonItem = UIBarButtonItem(customView: imageView)
        self.navigationItem.rightBarButtonItem = buttonItem
        //        apiCallforUserDetails()
        //        apiCallforStateDetails()
        
        apiCalls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        apiCalls()
    }
    
    func apiCalls() {
        // Alamofire, PromiseKit, SwiftyJSON
        getUserInfo()
            .done { json -> Void in
                // Do something with the JSON info
                let value = JSON(json)
                
                debugPrint(value["results"][0]["location"])
                
                for (_, data) in value["results"] {
                    
                    let userId = data["id"]["name"].string ?? ""
                    
                    if userId.count > 0 {
                        let title = data["name"]["title"].string ?? ""
                        let first = data["name"]["first"].string ?? ""
                        let last = data["name"]["last"].string ?? ""
                        print("Name Title \(title)")
                        print ("Name \(first)")
                        print ("Name \(last)")
                        
                        let city = data["location"]["city"].string ?? ""
                        let state = data["location"]["state"].string ?? ""
                        let country = data["location"]["country"].string ?? ""
                        let postcode = data["location"]["postcode"].int ?? 0
                        
                        let streetNumber = data["location"]["street"]["number"].int ?? 0
                        let streetName = data["location"]["street"]["name"].string ?? ""
                        
                        print ("streetNumber \(streetNumber)")
                        print ("street Name \(streetName)")
                        
                        print("City \(city)")
                        print ("Name \(state)")
                        print ("country \(country)")
                        print ("Name_Postcode \(postcode)")
                        
                        let email = data["email"].string ?? ""
                        let phone = data["phone"].string ?? ""
                        debugPrint(email)
                        debugPrint(phone)
                        debugPrint(data["location"]["street"])
                        
                        let id = data["id"]["name"].string ?? ""
                        let value = data["id"]["value"].string ?? ""
                        debugPrint(id)
                        debugPrint(value)
                        
                        let largeImage = data["picture"]["large"].string ?? ""
                        let mediumImage = data["picture"]["medium"].string ?? ""
                        let thumnbailImage = data["picture"]["thumbnail"].string ?? ""
                        debugPrint(largeImage)
                        debugPrint(mediumImage)
                        debugPrint(thumnbailImage)
                        
                        
                        self.userArray.append(UserModel(username: UserName(title: title, name: "\(first) \(last)"), location: Location(street: Street(number: String(streetNumber), name: streetName), city: city, state: state, country: country, postcode: String(postcode)), email: email, phone: phone, picture: Picture(large: largeImage, medium: mediumImage, thumbnail: thumnbailImage), id: Id(name: id, value: value)))
                        
                        self.userDetailArray.append(streetName)
                        self.userDetailArray.append(String(streetNumber))
                        self.userDetailArray.append(city)
                        self.userDetailArray.append(state)
                        self.userDetailArray.append(country)
                        self.userDetailArray.append(String(postcode))
                        self.userDetailArray.append(phone)
                        self.userDetailArray.append(email)
                        
                        if let userDet = self.userArray as [UserModel]?{
                            for row in 0..<userDet.count {
                                
                                self.detailCellText = [userDet[row].location.street.name,
                                                       userDet[row].location.street.number,
                                                       userDet[row].location.city,
                                                       userDet[row].location.state,
                                                       userDet[row].location.country,
                                                       userDet[row].location.postcode,
                                                       userDet[row].phone,
                                                       userDet[row].email]
                            }
                        }
                        self.tempDetailCellText = self.detailCellText
                        self.tableView.reloadData()
                    } else {
                        
                        let alert = UIAlertController(title: "ID not found", message: "Unable to retrieve data at this time, please try again after sometime", preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
                            print("User click Ok button")
                            self.apiCalls()
                            
                        }))
                        
                        self.present(alert, animated: true, completion: {
                            print("completion block")
                        })
                    }
                }
                
                debugPrint(value.dictionaryValue.values)
                
            }
            .catch { error in
                // Handle error or give feedback to the user
                debugPrint(error.localizedDescription)
            }
        
        getStateInfo()
            .done { json -> Void in
                // Do something with the JSON info
                let value = JSON(json)
                debugPrint(value.dictionaryValue)
                for (_, dataValues) in value["data"] {
                    
                    let state = dataValues["State"].string ?? ""
                    let id = dataValues["ID State"].string ?? ""
                    self.stateArray.append(StateModel(id: id, state: state))
                }
            }
            .catch { error in
                // Handle error or give feedback to the user
                debugPrint(error.localizedDescription)
            }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return userArray.count
        } else if section == 1 {
            if self.isRowSelected == true {
                if self.hiddenSections.contains(section) {
                    return userDetailArray.count
                }
                return userDetailArray.count + stateArray.count
            }
            return userDetailArray.count
        } else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
     
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 25))
            let label = UILabel()
            label.frame = CGRect.init(x: 0, y: 0, width: headerView.frame.width, height: headerView.frame.height)
            if section == 0 {
                label.text = "Profile & Settings"
            } else if section == 1 {
                label.text = "EDIT INFO"
            }
            label.font = .systemFont(ofSize: 16)
            label.textColor = .black
            label.textAlignment = .center
            label.backgroundColor = .white
            headerView.addSubview(label)
            return headerView
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReusableCellOne", for: indexPath) as! DetailsTableViewCell
            if userArray.count > 0 {
                cell.labelOne.text = "\(userArray[indexPath.row].username.title) \(userArray[indexPath.row].username.name) "//"John Peter"
                cell.labelTwo.text = userArray[indexPath.row].id.value//"123456"
                cell.labelThree.text = userArray[indexPath.row].id.name //"john.peter"
                cell.labelFour.text = userArray[indexPath.row].phone//"123-456-7891"
                cell.labelFive.text = userArray[indexPath.row].email//"johnpeter@gmail.com"
                cell.contactImage.downloaded(from: userArray[indexPath.row].picture.large)
                
            }
            return cell
            
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReusableCell", for: indexPath)
                as! MessageCell
            
            if indexPath.row < tempMessageCellText.count {
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.hideSection(_:)))
                
                cell.addGestureRecognizer(tapGesture)
                
                cell.label.text = tempMessageCellText[indexPath.row]
                
                cell.textfield.text = tempDetailCellText[indexPath.row]
                
                cell.textfield.isHidden = false
                
                cell.messageBubble.backgroundColor =  #colorLiteral(red: 0.5921568627, green: 0.6745098039, blue: 0.862745098, alpha: 1)
                
                if tempDetailCellText[indexPath.row] == "true" {
                    
                    cell.textfield.isHidden = true
                    cell.messageBubble.backgroundColor = .white
                }
            }
            return cell
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 1 {
            if (self.tableView.cellForRow(at: indexPath) as? MessageCell) != nil {
                if indexPath.row  == stateIndexPath {
                    isRowSelected = true
                }
            }
        }
        
    }
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            if (self.tableView.cellForRow(at: indexPath) as? MessageCell) != nil {
                if indexPath.row  == stateIndexPath {
                    isRowSelected = false
                }
            }
        }
    }
    
    
    
    @objc
    private func hideSection(_ sender: UITapGestureRecognizer? = nil) {
        
        let section = 1
        var tappedIndexPath = 0
        var tappedTF = ""
        var changedState = ""
        var hasChangedState = false
        
        if sender?.state == UIGestureRecognizer.State.ended {
            guard let tapLocation = sender?.location(in: self.tableView) else { return  }
            if let tapIndexPath = self.tableView.indexPathForRow(at: tapLocation) {
                if let tappedCell = self.tableView.cellForRow(at: tapIndexPath) as? MessageCell {
                    
                    //do what you want to cell here
                    
                   tappedIndexPath = tapIndexPath.row
                    
                    if tappedCell.textfield.text != nil {
                        tappedTF = tappedCell.textfield.text ?? ""
                        if tappedTF == "true" {
                            changedState = tappedCell.label.text ?? ""
                            hasChangedState = true
                        }
                    }
                }
            }
        }
        func indexPathsForSection() -> [IndexPath] {
            
            var indexPaths = [IndexPath]()
            
            if stateArray.count > 0 {
                if let states = stateArray as [StateModel]?{
                    if !isRowInserted{
                        for row in 1..<self.stateArray.count+1 {
                            indexPaths.append(IndexPath(row: stateIndexPath+row, section: section))
                            tempDetailCellText = detailCellText
                            tempMessageCellText = messageCellText
                            if hasChangedState {
                                tempDetailCellText[stateIndexPath] = changedState
                            }
                        }
                        
                    } else {
                        for row in 1..<self.stateArray.count+1 {
                            indexPaths.append(IndexPath(row: stateIndexPath+row, section: section))
                            
                            tempMessageCellText.insert(states[row-1].state, at: stateIndexPath+row)
                            
                            tempDetailCellText.insert(isState, at: stateIndexPath+row)
                        }
                    }
                }
            }
            return indexPaths
        }
        
        if tappedIndexPath == stateIndexPath || tappedTF == "true" {
            if !isRowInserted {
                self.hiddenSections.remove(section)
                isRowSelected = true
                isRowInserted = true
                self.tableView.insertRows(at: indexPathsForSection(), with: .fade)
                
            } else {
                
                self.hiddenSections.insert(section)
                isRowSelected = false
                isRowInserted = false
                self.tableView.deleteRows(at: indexPathsForSection(), with: .automatic)
            }
        }
        self.tableView.reloadData()
    }
}


extension ProfileViewController {
    
    // let url = "https://randomuser.me/api/"
    // let url = "https://datausa.io/api/data"
    
    func getUserInfo() -> Promise<[String: Any]> {
        return Promise { seal in
            
            let url = "https://randomuser.me/api/"
            var parameters: Parameters = [:]
            
            parameters = [
                "exc": "login,dob,registered,gender,nat,cell,timezone"
            ]
            AF.request(
                url,
                method: .get,
                parameters: parameters,
                encoding: URLEncoding.default,
                headers: nil).responseJSON { response in
                    
                    switch response.result {
                    case .success(let json):
                        guard let json = json  as? [String: Any] else {
                            return seal.reject(AFError.responseValidationFailed(reason: .dataFileNil))
                        }
                        seal.fulfill(json)
                    case .failure(let error):
                        seal.reject(error)
                    }
                }
        }
    }
    
    func getStateInfo() -> Promise<[String: Any]> {
        return Promise { seal in
            
            let url = "https://datausa.io/api/data"
            var parameters: Parameters = [:]
            parameters = [
                "drilldowns": "State",
                "measures": "Population",
                "limit":"5"
            ]
            AF.request(
                url,
                method: .get,
                parameters: parameters,
                encoding: URLEncoding.default,
                headers: nil).responseJSON { response in
                    
                    switch response.result {
                    case .success(let json):
                        guard let json = json  as? [String: Any] else {
                            return seal.reject(AFError.responseValidationFailed(reason: .dataFileNil))
                        }
                        seal.fulfill(json)
                    case .failure(let error):
                        seal.reject(error)
                    }
                }
        }
    }
}

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
            else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}
