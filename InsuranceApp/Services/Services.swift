//
//  DetailsViewController.swift
//  InsuranceApp
//
//  Created by Thulasipathi Goalla on 8/19/21.
//

import Foundation
import Alamofire

typealias ServiceResponse = (NSDictionary?, NSError?,Int) -> Void

class Services: NSObject {
    
    var baseUrl = "https://reqres.in/api"
    
    class var sharedInstance : Services {
        struct Singleton {
            static let instance = Services()
        }
        return Singleton.instance
    }
    
    func alamoFirePostRequestWithUrl(jsonBody: [String: AnyObject], subUrl: String, onCompletion: @escaping ServiceResponse) -> Void{
        
        print(jsonBody as Any)
        
        let url = (URL(string: baseUrl+subUrl))! as URL
        
        let jsonData = try! JSONSerialization.data(withJSONObject: jsonBody, options: [])
        
        var request = URLRequest(url: url as URL)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")

        request.timeoutInterval = 30
        print(request as Any)
        request.httpMethod = "post"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonData
        
        AF.request(request).responseJSON { (dataResponse) in
            
            print(dataResponse.response as Any)
            print(dataResponse.result)
            print(dataResponse.error as Any)
            
            let httpresponse = dataResponse.response
            
            if let error = dataResponse.error {
                print("error:", error.localizedDescription)
                onCompletion(nil, error as NSError?,httpresponse?.statusCode ?? 0)
                return
            }
            do {
                guard let data = dataResponse.data else { return }
                if httpresponse?.statusCode == 200 {
                    guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] else { return }
                    print("AlamoFireResponse - \(json as Any)")
                    onCompletion(NSDictionary(dictionary: json), nil, httpresponse?.statusCode ?? 0)
                } else {
                    guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] else { return }
                    print("AlamoFireResponse - \(json as Any)")
                    onCompletion(NSDictionary(dictionary: json), nil,(httpresponse?.statusCode)!)
                }
            } catch {
                onCompletion(nil, error as NSError?,httpresponse?.statusCode ?? 0)
            }
        }
    }
    
}
