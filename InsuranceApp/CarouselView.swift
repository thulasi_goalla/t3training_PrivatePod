//
//  CarouselView.swift
//  InsuranceApp
//
//  Created by Thulasipathi Goalla on 8/24/21.
//

import UIKit

class CarouselView: UIView {

    let imageView: UIImageView = {
       let imgV = UIImageView()
        return imgV
    }()
    

    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        setUpView()
        setUpLayout()
    }
    
    private func setUpView() {
        addSubview(imageView)
    }
    
    private func setUpLayout() {

        self.addSubview(imageView)
        imageView.snp.makeConstraints { (make) in
            make.topMargin.equalTo(10)
            make.bottomMargin.equalTo(-10)
            make.leftMargin.equalTo(10)
            make.rightMargin.equalTo(-10)
        }
    }
}
