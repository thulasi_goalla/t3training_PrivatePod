//
//  ViewController.swift
//  InsuranceApp
//
//  Created by Subrahmanya Kasanna on 8/9/21.
//

import UIKit
import SnapKit
import PromiseKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupView()
        testPods()
    }
    
    func testPods() {
        // Alamofire, PromiseKit, SwiftyJSON
        getUserInfo()
            .done { json -> Void in
                // Do something with the JSON info
                let value = JSON(json)
                debugPrint(value.dictionaryValue)
            }
            .catch { error in
                // Handle error or give feedback to the user
                debugPrint(error.localizedDescription)
            }
    }
    
    private func setupView() {
        title = "State Farm"
        view.backgroundColor = .black
        
        let buttonViewProfile: UIButton = {
            let button = UIButton.init(type: .custom)
            button.setTitle("Profile", for: .normal)
            button.backgroundColor = .gray
            button.addTarget(self, action: #selector(showProfile(sender:)), for: .touchUpInside)
            return button
        }()
        
        view.addSubview(buttonViewProfile)
        buttonViewProfile.snp.makeConstraints {
            $0.centerX.centerY.equalToSuperview()
            $0.width.equalToSuperview().inset(30)
            $0.height.equalTo(50)
        }
    }
    
    @objc
    private func showProfile(sender: UIButton) {
        let profileViewController = ProfileViewController()
        navigationController?.pushViewController(profileViewController, animated: true)
    }
}

extension ViewController {
    func getUserInfo() -> Promise<[String: Any]> {
        return Promise { seal in
            AF.request("https://jsonplaceholder.typicode.com/users/1")
                .validate()
                .responseJSON { response in
                    
                    switch response.result {
                    case .success(let json):
                        guard let json = json  as? [String: Any] else {
                            return seal.reject(AFError.responseValidationFailed(reason: .dataFileNil))
                        }
                        seal.fulfill(json)
                    case .failure(let error):
                        seal.reject(error)
                    }
                }
        }
    }
}

